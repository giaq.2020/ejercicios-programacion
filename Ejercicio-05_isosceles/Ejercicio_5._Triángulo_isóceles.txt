Inicio
    //Escribir "Ingrese la longitud de los lados iguales del triángulo"
    Leer longitud_lados_iguales

    //Escribir "Ingrese la longitud del lado diferente del triángulo"
    Leer longitud_lado_diferente

    // Calcular el perímetro del triángulo isósceles
        perimetro = 2 * longitud_lados_iguales + longitud_lado_diferente
        
    // Mostrar el resultado
        Escribir "El perímetro del triángulo isósceles, " es: ", perimetro
    
Fin