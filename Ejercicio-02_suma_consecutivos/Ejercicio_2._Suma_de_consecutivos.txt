Inicio
    Escribir "Ingrese un número entre 1 y 50"
    Leer número

    // Comprobar que el número está en el rango:
    Si número >= 1 y número <= 50 entonces
        // Iniciar proceso suma
        suma = 0
        contador = 1
        
        // Calcular la suma de los números consecutivos
        Mientras contador <= numero
            suma = suma + contador
            contador = contador + 1
        Fin Mientras
        
        // Mostrar resultado
        Escribir "La suma de los números consecutivos del 1 hasta el ", numero, " es: ", suma
    Sino
        Escribir "El número ingresado no está en el rango."
    Fin Si
Fin